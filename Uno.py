FILENAME = "co2_emission.csv"

(PAIS,
 CODIGO,
 ANIO,
 CO2) = range(4)

def openfile():
    temp = open(FILENAME)
    
    diccionario = {}
    contador = 0
    pais_fa = "Afghanistan"
    cu_anterior = 0
    cuenta = 0
            
    for contador, linea in enumerate(temp): #asimila filas en listas
        if contador != 0: #para no considerar el enunciado
            temp_linea = linea.split(",")
            pais = temp_linea[PAIS].strip()
            codigo = temp_linea[CODIGO].strip()
            anio = temp_linea[ANIO].strip()
            co2 = temp_linea[CO2].strip()

            temp_dic = {"codigo": codigo,
                        anio: co2}
  
            if diccionario.get(pais):
                diccionario[pais].update(temp_dic)
            else:
                diccionario[pais] = temp_dic
                        
            if pais == pais_fa:  #pais_fa = pais fila anterior
                cuenta += 1
                
            elif pais != pais_fa:
                if cuenta > cu_anterior:
                    cu_anterior = cuenta
                    cumayor = cuenta  #contador mayor de pais()
                    mapais = pais_fa     #para imprimir pais mayor
                cuenta = 0

            pais_fa = pais  #define el pais "actual" como pais anterior para el otro pais siguiente           
 
    temp.close()
    return mapais,cumayor

def main():
    mapais,cumayor = openfile()
    print(f"\nEl pais con mayor cantidad de registros es {mapais} con {cumayor} registros\n")

main()
