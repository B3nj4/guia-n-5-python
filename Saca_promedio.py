FILENAME = "co2_emission.csv"

(PAIS,
 CODIGO,
 ANIO,
 CO2) = range(4)

def ingreso_pais():
    ingrese = input("\nIngrese nombre de un pais: ")
    return ingrese 

def openfile(ingrese):   
    temp = open(FILENAME)
    diccionario = {}
    suma = 0
    cuenta = 0

    for counter, linea in enumerate(temp):
        if counter != 0:
            temp_linea = linea.split(",")
            pais = temp_linea[PAIS].strip()
            codigo = temp_linea[CODIGO].strip()
            anio = temp_linea[ANIO].strip()
            co2 = temp_linea[CO2].strip()

            temp_dic = {"codigo": codigo,
                        anio: co2}

            if diccionario.get(pais):
                diccionario[pais].update(temp_dic)
            else:
                diccionario[pais] = temp_dic
           
            if pais == ingrese.capitalize():
                suma = suma + float(co2)
                cuenta += 1                      
    
    promedio = suma/cuenta
    print("\n(Promedio = Suma de las toneladas de Co2 / Total de registros)")
    print(f"Promedio de {ingrese}: {promedio} || Total de registros de Co2: {cuenta} || Suma de toneladas de Co2: {suma}\n")
    

    temp.close()

def main():
    ingrese = ingreso_pais()
    openfile(ingrese)

main()
