FILENAME = "co2_emission.csv"

(PAIS,
 CODIGO,
 ANIO,
 CO2) = range(4)

def menu():
    menu = [" "] * 4
    menu[0] = "--------------------------------------------------------------------"
    menu[1] = "   Presione(1) para buscar un pais en específico y su informacion   "
    menu[2] = "   Presione(2) para cerrar el programa                              "
    menu[3] = "--------------------------------------------------------------------"

    for fila in menu:
        texto = "< "
        texto = texto + fila + " "
        print(texto + " >") 

def ingreso_pais():
    ingrese = input("Ingrese nombre de algún país: ")
    return ingrese    

def openfile(ingrese):
    temp = open(FILENAME)
    diccionario = {}

    for counter, linea in enumerate(temp):
        if counter != 0:
            temp_linea = linea.split(",")
            
            pais = temp_linea[PAIS].strip()
            codigo = temp_linea[CODIGO].strip()
            anio = temp_linea[ANIO].strip()
            co2 = temp_linea[CO2].strip()

            temp_dic = {"codigo": codigo,
                        anio: co2}

            if diccionario.get(pais):
                diccionario[pais].update(temp_dic)
            else:
                diccionario[pais] = temp_dic  

            if pais == ingrese.capitalize():
                print(f"País: {pais} || Codigo: {codigo} || Año: {anio} || Emision: {co2}")  
   
    temp.close() 

def intera_menu():
    
    while True:
        print("\n")
        menu()
        tecla = input("\nPresione(número) para elegir alguna opción mostrada en el menú: ")
        
        if tecla == "1":
            ingrese = ingreso_pais()
            openfile(ingrese)
                        
        elif tecla == "2":
            print("\n\nChao Mundo\n    ...\n")
            break

intera_menu()
