FILENAME = "co2_emission.csv"

(PAIS,
 CODIGO,
 ANIO,
 CO2) = range(4)

def openfile():    #openfile con datas exclusivas para sacar promedio general
    temp = open(FILENAME)
    diccionario = {}
    suma = 0
    cuenta = 0

    for counter, linea in enumerate(temp):
        if counter != 0:
            temp_linea = linea.split(",")
            pais = temp_linea[PAIS].strip()
            codigo = temp_linea[CODIGO].strip()
            anio = temp_linea[ANIO].strip()
            co2 = temp_linea[CO2].strip()

            temp_dic = {"codigo": codigo,
                        anio: co2}

            if diccionario.get(pais):
                diccionario[pais].update(temp_dic)
            else:
                diccionario[pais] = temp_dic
            
            suma = suma + float(co2)  #la suma de la cantidad de emisiones respecto a cada pais
            cuenta += 1     #cuenta la cantidad de paises, en este caso, la totalidad de los paises

    temp.close()
    return suma,cuenta

def promedio_general(suma,cuenta):
    pgeneral = suma / cuenta      #promedio general
    return pgeneral

def rango(pgeneral):
    porcentaje = (pgeneral * 10) / 100
    izquierda = pgeneral - porcentaje
    derecha = pgeneral + porcentaje
    return porcentaje,izquierda,derecha

def promedio_pais(pgeneral,izquierda,derecha,porcentaje):  #openfilo que saque los datas cruciales para sacar promedio por cada pais                                  
    temp = open(FILENAME)
    diccionario = {}
    paises_rango = {}
    suma = 0
    cuenta = 0
    pais_fa = "Afghanistan"
    
    for counter, linea in enumerate(temp):
        if counter != 0:
            temp_linea = linea.split(",")
            pais = temp_linea[PAIS].strip()
            codigo = temp_linea[CODIGO].strip()
            anio = temp_linea[ANIO].strip()
            co2 = temp_linea[CO2].strip()

            temp_dic = {"codigo": codigo,
                        anio: co2}

            if diccionario.get(pais):
                diccionario[pais].update(temp_dic)
            else:
                diccionario[pais] = temp_dic
            
            if pais == pais_fa:  #pais_fa = pais fila anterior
                suma = suma + float(co2)
                cuenta += 1    #cuenta la cantidad que se repite un pais,esto se valida por la condicion siguiente                              
            elif pais != pais_fa:
                promedio_x = suma / cuenta   #promedio de x pais (el que entre por el for)
                if izquierda <= promedio_x <= derecha:
                    paises_rango[pais] = promedio_x  #guarda el pais en un diccionario con key=pais y value=promdeio emitido en toneladas
                cuenta = 1 
                suma = float(co2)
                                            
            pais_fa = pais

    temp.close()
    return paises_rango

def main():
    suma,cuenta = openfile()
    pgeneral = promedio_general(suma,cuenta)
    porcentaje,izquierda,derecha = rango(pgeneral)
    paises_rango = promedio_pais(porcentaje,izquierda,derecha,pgeneral)
    print(f"\n(Promedio total: {pgeneral} || Zona Izquierda: {izquierda} || Zona Derecha: {derecha})")
    print(f"\nPais/es con promedio dentro de un rango del +-10% aledaños al promedio son/es:\n{paises_rango}\n")
    
main()
