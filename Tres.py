FILENAME = "co2_emission.csv"

(PAIS,
 CODIGO,
 ANIO,
 CO2) = range(4)

def openfile():
    temp = open(FILENAME)
    diccionario = {}
    suma = 0
    cuenta = 0

    for counter, linea in enumerate(temp):
        if counter != 0:
            temp_linea = linea.split(",")
            pais = temp_linea[PAIS].strip()
            codigo = temp_linea[CODIGO].strip()
            anio = temp_linea[ANIO].strip()
            co2 = temp_linea[CO2].strip()


            temp_dic = {"codigo": codigo,
                        anio: co2}

            if diccionario.get(pais):
                diccionario[pais].update(temp_dic)
            else:
                diccionario[pais] = temp_dic
            
            suma = suma + float(co2)  #la suma de la cantidad de emisiones respecto a cada pais
            cuenta += 1     #cuenta la cantidad de paises (similar a len()-1 que no considere el enunciado)
                        
    temp.close()
    return suma,cuenta

def promedio_sacado(suma,cuenta):
    promedio = suma / cuenta
    return promedio

def main():
    suma,cuenta = openfile()
    promedio = promedio_sacado(suma,cuenta)
    print(f"\nEl promedio correspondiente de todos los paises es {promedio} toneladas de Co2\n")

main()
