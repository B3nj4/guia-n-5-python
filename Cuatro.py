FILENAME = "co2_emission.csv"

(PAIS,
 CODIGO,
 ANIO,
 CO2) = range(4)

def openfile():
    temp = open(FILENAME)
    
    diccionario = {}
    suma = 0
    cuenta = 0
    pais_fa = "Afghanistan"  #para que inicie con el primer pais que es afgha
    promedio_alto = 0

    for counter, linea in enumerate(temp):
        if counter != 0:
            temp_linea = linea.split(",")
            pais = temp_linea[PAIS].strip()
            codigo = temp_linea[CODIGO].strip()
            anio = temp_linea[ANIO].strip()
            co2 = temp_linea[CO2].strip()

            temp_dic = {"codigo": codigo,
                        anio: co2}

            if diccionario.get(pais):
                diccionario[pais].update(temp_dic)
            else:
                diccionario[pais] = temp_dic

            if pais == pais_fa:  #pais_fa = pais fila anterior
                suma = suma + float(co2)
                cuenta += 1
                
            elif pais != pais_fa:
                promedio_x = suma / cuenta   #promedio de x pais (el que entre por el for)
                if promedio_x > promedio_alto:
                    promedio_alto = promedio_x     #se define el promedio mas alto para poder imprimirla
                    pais_alto = pais_fa            #pais que es el de promedio mas alto 
                cuenta = 1
                suma = float(co2)   

            pais_fa = pais  #define el pais "actual" como pais anterior para el otro pais siguiente             
                        
    temp.close()
    return promedio_alto,pais_alto 

def main():
    promedio_alto,pais_alto = openfile()
    print(f"\nEl promedio de toneladas de Co2 por pais más alto es {pais_alto} con un promedio de {promedio_alto} toneladas del gas\n")

main()
